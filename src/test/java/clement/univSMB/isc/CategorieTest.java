package clement.univSMB.isc;

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import clement.univSMB.isc.model.Categorie;
import clement.univSMB.isc.model.Plat;


public class CategorieTest {
	
	Categorie cat;
	ArrayList<Plat> listePlats;
	ArrayList<String> listePlatsString;
	
	@Before
	public void setUp() {
		cat = new Categorie("Boissons");
	}
	
	@Test
	public void shouldHaveAName() throws Exception {
		String name = cat.getNomCategorie();
	}
	
	@Test
	public void shouldAddADish() throws Exception {
		cat.ajouterPlat("Flammekueche", 10.50f);
	}
	
	@Test
	public void shouldHaveADish() throws Exception {
		listePlats = cat.getListePlats();
	}

	@Test
	public void shouldHaveAListOfDishesToString() throws Exception {
		listePlatsString = cat.consulterListePlats();
	}
}
