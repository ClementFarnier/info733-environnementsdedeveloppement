package clement.univSMB.isc.ihm;

import javax.swing.JMenuItem;

import clement.univSMB.isc.model.Table;

public class TableMenuItem extends JMenuItem {
	private static final long serialVersionUID = 1L;
	private Table tableLiee;
	
	public TableMenuItem(Table tableLiee)
	{
		super(tableLiee.toString());
		this.tableLiee = tableLiee;
	}
	
	public Table getTableLiee()
	{
		return this.tableLiee;
	}
}