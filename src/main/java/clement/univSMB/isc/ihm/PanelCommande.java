package clement.univSMB.isc.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import clement.univSMB.isc.model.*;

public class PanelCommande extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JLabel lblSelection;
	private JLabel lblCommande;
	private JTextField tfSelection;
	private DefaultListModel<String> lModel;
	private JList jListCommande;
	private JButton btnAjouter;
	private JLabel lblTable;
	private IHMService p;
	private JButton btnFermer;
	
	private Menu menuSelect;
	private Plat platSelect;
	
	private Table tCmd;
	private Commande cmd;
	private boolean cmdValidee;
	private JButton btnValider;
	private JButton btnVoirTickets;
	private JButton btnDemanderSuite;
	
	public PanelCommande(IHMService parent) {
		this.p = parent;
		setBounds(640, 20, 320, 440);
		setLayout(null);
		
		lblSelection = new JLabel("Selection : ");
		lblSelection.setBounds(10, 36, 79, 14);
		add(lblSelection);
		
		lblTable = new JLabel("Table");
		lblTable.setBounds(10, 11, 235, 14);
		add(lblTable);
		
		lModel = new DefaultListModel<String>();
		jListCommande = new JList(lModel);
		jListCommande.setBounds(10, 106, 300, 212);
		add(jListCommande);
		
		btnAjouter = new JButton("Ajouter >>");
		btnAjouter.addActionListener(this);
		btnAjouter.setBounds(10, 78, 110, 23);
		add(btnAjouter);
		
		lblCommande = new JLabel("Commande :");
		lblCommande.setBounds(139, 82, 101, 14);
		add(lblCommande);
		
		tfSelection = new JTextField();
		tfSelection.setEditable(false);
		tfSelection.setBounds(90, 33, 216, 20);
		add(tfSelection);
		tfSelection.setColumns(10);
		
		btnFermer = new JButton("<< Fermer");
		btnFermer.setBounds(10, 406, 114, 23);
		btnFermer.addActionListener(this);
		add(btnFermer);
		
		btnValider = new JButton("Valider commande");
		btnValider.addActionListener(this);
		btnValider.setBounds(134, 344, 176, 23);
		add(btnValider);
		
		btnVoirTickets = new JButton("Voir tickets");
		btnVoirTickets.addActionListener(this);
		btnVoirTickets.setBounds(154, 344, 156, 23);
		add(btnVoirTickets);
		
		btnDemanderSuite = new JButton("Demander suite");
		btnDemanderSuite.addActionListener(this);
		btnDemanderSuite.setBounds(152, 405, 156, 25);
		btnDemanderSuite.setEnabled(false);
		add(btnDemanderSuite);
	}
	
	private void reloadJListCommande()
	{
		lModel.removeAllElements();
		for(Plat p: cmd.getListePlats())
		{
			lModel.addElement(p.getInfosPlat());
		}
		for(Menu m: cmd.getListeMenus())
		{
			lModel.addElement(m.getNomMenu());
		}
	}

	public void reloadTable()
	{
		this.setTableSelectionee(this.tCmd);
	}

	public void setTableSelectionee(Table t)
	{
		if(cmd != null && cmdValidee == false)
		{
			//Si la commande précédente n'a pas été validée, on l'annule
			annulerCommande();
		}
		
		this.tCmd = t;
		this.lblTable.setText(t.toString());
		if(t.estLibre())
		{
			this.cmd = t.initialiserCommande();
			cmdValidee = false;
			this.btnValider.setVisible(true);
			this.btnVoirTickets.setVisible(false);
			this.btnDemanderSuite.setEnabled(false);
		}
		else
		{
			this.cmd = t.getCommande();
			cmdValidee = true; //une commande existante etait dejà validée
			this.btnValider.setVisible(false);
			this.btnVoirTickets.setVisible(true);

			if(cmd.getTickets().size() > 0)
			{
				btnDemanderSuite.setEnabled(true);
			}
		}
		reloadJListCommande();
	}
	
	public void setSelection(Plat p)
	{
		this.platSelect = p;
		this.menuSelect = null;
		this.tfSelection.setText(p.getInfosPlat());
	}
	
	public void setSelection(Menu m)
	{
		this.menuSelect = m;
		this.platSelect = null;
		this.tfSelection.setText(m.getNomMenu() + " - " + m.getPrixMenu() + "€");
	}
	
	private void creerTicketsAuto(ArrayList<Plat> lObjetsCommandes)
	{
		//Temporaire en attendant de le faire via l'ihm
		ArrayList<Plat> lEntrees = new ArrayList<Plat>();
		ArrayList<Plat> lBoissons = new ArrayList<Plat>();
		ArrayList<Plat> lPlats = new ArrayList<Plat>();
		ArrayList<Plat> lDesserts = new ArrayList<Plat>();
		
		for(Plat plat : lObjetsCommandes)
		{
			if(plat.getCategorieParente().getNomCategorie().equals("Entrées"))
			{
				lEntrees.add(plat);
			}
			else if(plat.getCategorieParente().getNomCategorie().equals("Desserts"))
			{
				lDesserts.add(plat);
			}
			else if(plat.getCategorieParente().getNomCategorie().equals("Boissons"))
			{
				lBoissons.add(plat);
			}
			else //Tous les autres plats
			{
				lPlats.add(plat);
			}
		}
		if(lEntrees.size() > 0)
		{
			cmd.creerTicket(p.getApp(), lEntrees, Ticket.TypeTicket.TICKET_CUISINE);
		}
		if(lBoissons.size() > 0)
		{
			cmd.creerTicket(p.getApp(), lBoissons, Ticket.TypeTicket.TICKET_BAR);
		}
		if(lPlats.size() > 0)
		{
			cmd.creerTicket(p.getApp(), lPlats, Ticket.TypeTicket.TICKET_CUISINE);
		}
		if(lDesserts.size() > 0)
		{
			cmd.creerTicket(p.getApp(), lDesserts, Ticket.TypeTicket.TICKET_CUISINE);
		}
	}
	
	private void annulerCommande()
	{
		tCmd.supprimerCommande();
		p.getPanelPrincipal().updateListesTables(); //update liste tables vides/occup
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnFermer)
		{
			if( !cmdValidee )
			{
				annulerCommande();
			}
			this.setVisible(false);
			p.setSize(640, 480);
			p.getPanelPrincipal().setSize(640,480);
		}
		else if(e.getSource() == btnAjouter)
		{
			if(this.menuSelect != null)
			{
				cmd.ajouterMenu(menuSelect);
			}
			else if(this.platSelect != null) //else if car il est possible que rien ne soit select
			{
				cmd.ajouterPlat(platSelect);
			}
			lModel.addElement(tfSelection.getText());
		}
		else if(e.getSource() == btnValider)
		{
			if( ! cmdValidee )
			{
				cmdValidee = true;
				creerTicketsAuto( cmd.validerCommande() );
				cmd.finaliserCommande();
				p.getPanelPrincipal().updateListesTables(); //update liste tables vides/occup
				this.btnValider.setVisible(false);
				this.btnVoirTickets.setVisible(true);
				this.btnDemanderSuite.setEnabled(true);
				JOptionPane.showMessageDialog(this, "Commande validée !");
			}
		}
		else if(e.getSource() == btnVoirTickets)
		{
			if(cmd.getTickets().size() > 0)
			{
				JOptionPane.showMessageDialog(this, cmd.getInfosTickets());
			}
			else
			{
				JOptionPane.showMessageDialog(this, "Commande terminée.");
			}
		}
		else if(e.getSource() == btnDemanderSuite)
		{
			//Tests pour des raisons d'ergonomie
			//Plus de tickets
			if(cmd.getTickets().size() == 0)
			{
				JOptionPane.showMessageDialog(this, "Tous les tickets ont été préparés.");
			}

			else
			{
				boolean ticketEnvoye = cmd.demanderSuite();

                if(ticketEnvoye == false)
                {
                    JOptionPane.showMessageDialog(this, "Un ticket est déjà en préparation :\n" + cmd.getTickets().get(0));
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Ticket envoyé en préparation : \n"
                        + cmd.getTickets().get(0) );
                }
			}
		}
		
	}
}