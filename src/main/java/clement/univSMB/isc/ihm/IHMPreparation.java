package clement.univSMB.isc.ihm;

import clement.univSMB.isc.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class IHMPreparation extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

    protected Application app;
    protected IHM ihm;
    protected Container c;
    protected JLabel lblNouveauTicket;
    protected DefaultListModel<String> lModel;
    protected JList jListPlats;
    protected JButton btnPret, btnAccepter;

    protected Ticket ticketEnvoye;

	public IHMPreparation(IHM ihm)
    {
        this.ihm = ihm;
        this.app = ihm.getApp();
	    this.setBounds(640, 0, 270, 290);
	    this.setResizable(false);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.c = this.getContentPane();
	    this.c.setLayout(null);
        this.ticketEnvoye = null;
	    
	    lblNouveauTicket = new JLabel("Ticket suivant :");
	    lblNouveauTicket.setBounds(12, 12, 187, 15);
	    getContentPane().add(lblNouveauTicket);

        lModel = new DefaultListModel<String>();
        lModel.addElement("Aucun ticket en attente.");
	    jListPlats = new JList(lModel);
	    jListPlats.setBounds(12, 39, 246, 178);
	    getContentPane().add(jListPlats);
	    
	    btnAccepter = new JButton("Accepter");
	    btnAccepter.setBounds(12, 229, 117, 25);
		btnAccepter.addActionListener(this);
	    getContentPane().add(btnAccepter);
	    
	    btnPret = new JButton("Prêt");
	    btnPret.setBounds(141, 229, 117, 25);
		btnPret.addActionListener(this);
	    getContentPane().add(btnPret);
		btnPret.setEnabled(false);

		this.setVisible(true);
	}

    public abstract void actualiserTicketEnvoye();

    protected void majInterfaceNouveauTicket()
    {
        System.out.println("IHMPreparation.majInterfaceNouveauTicket");
        lModel.removeAllElements();
        if(ticketEnvoye != null)
        {
            for (Plat p : ticketEnvoye.getListePlats()) {
                lModel.addElement(p.getNomPlat());
            }
            btnAccepter.setEnabled(true);
        }
        else
        {
            lModel.addElement("Aucun ticket en attente.");
            btnAccepter.setEnabled(false);
        }
    }

    public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == btnAccepter)
        {
            if(ticketEnvoye != null)
            {
                btnAccepter.setEnabled(false);
                btnPret.setEnabled(true);
                //Le ticket passe "en préparation"
                ticketEnvoye.getEtat().annoterTicket();
            }
        }
        else if(e.getSource() == btnPret)
        {
            btnAccepter.setEnabled(true);
            btnPret.setEnabled(false);
            //Le ticket passe "pret"
            ticketEnvoye.getEtat().annoterTicket();
            ticketEnvoye = null;
            actualiserTicketEnvoye();
        }

	}
}