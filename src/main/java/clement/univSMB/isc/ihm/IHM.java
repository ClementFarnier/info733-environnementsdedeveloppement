package clement.univSMB.isc.ihm;


import clement.univSMB.isc.model.Application;

public class IHM {
    private IHMService ihmService;
    private IHMCaisse ihmCaisse;
    private IHMCuisine ihmCuisine;
    private IHMBar ihmBar;

    private Application app;

    public IHM(Application app)
    {
        this.app = app;
        app.setIHM(this);
        ihmCuisine = new IHMCuisine(this);
        ihmBar = new IHMBar(this);
        ihmBar.setBounds(910, 0, 270, 290);
        ihmCaisse = new IHMCaisse(this);
        ihmService = new IHMService(this);
    }

    public Application getApp() {
        return app;
    }

    public IHMService getIhmService() {
        return ihmService;
    }

    public IHMCaisse getIhmCaisse() {
        return ihmCaisse;
    }

    public IHMCuisine getIhmCuisine() {
        return ihmCuisine;
    }

    public IHMBar getIhmBar() {
        return ihmBar;
    }
}
