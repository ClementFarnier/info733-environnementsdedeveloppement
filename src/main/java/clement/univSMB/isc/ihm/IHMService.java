package clement.univSMB.isc.ihm;

import java.awt.Container;
import java.util.ArrayList;

import javax.swing.*;

import clement.univSMB.isc.model.*;

public class IHMService extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private Application app;
	private IHM ihm;
	private Container c;
	private PanelPrincipal panelPrincipal;
	private Table tableSelectionnee;

	public IHMService(IHM ihm)
	{
		this.app = app;
		this.app = ihm.getApp();
		this.setTitle("A La Bonne Franquette (Service)");
	    this.setBounds(0, 0, 640, 490);
	    this.setResizable(false);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.c = this.getContentPane();
	    this.c.setLayout(null);
	    
	    this.panelPrincipal = new PanelPrincipal(this);
	    c.add(panelPrincipal);

	    this.setVisible(true);
	}
	
	public PanelPrincipal getPanelPrincipal() {
		return panelPrincipal;
	}

	public Application getApp() {
		return app;
	}

	public void notifierTicketPret(Ticket t)
	{
		JOptionPane.showMessageDialog(this, "Un ticket est prêt pour la " + t.getCmd().getTable()
                                        + "\n" + t.toString() );
	}

	public Table getTableSelectionnee() {
		return tableSelectionnee;
	}

	public void setTableSelectionnee(Table tableSelectionnee) {
		this.tableSelectionnee = tableSelectionnee;
	}
	
}