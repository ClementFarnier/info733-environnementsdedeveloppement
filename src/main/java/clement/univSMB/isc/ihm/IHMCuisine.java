package clement.univSMB.isc.ihm;

import clement.univSMB.isc.model.Plat;

public class IHMCuisine extends IHMPreparation {

    public IHMCuisine(IHM ihm)
    {
        super(ihm);
        this.setTitle("ALBF (Cuisine)");
    }

    public void actualiserTicketEnvoye() {
        System.out.println("IHMCuisine.actualiserTicketEnvoye");

        //Si on n'est pas en train de préparer un ticket on en prend un nouveau
        if(this.ticketEnvoye == null) {
            this.ticketEnvoye = app.getPremierTicketFileCuisine();
            this.majInterfaceNouveauTicket();
        }
    }


}
