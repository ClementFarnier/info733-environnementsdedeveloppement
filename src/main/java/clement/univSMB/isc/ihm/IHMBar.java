package clement.univSMB.isc.ihm;

import clement.univSMB.isc.model.Plat;

public class IHMBar extends IHMPreparation {

    public IHMBar(IHM ihm)
    {
        super(ihm);
        this.setTitle("ALBF (Bar)");
    }

    public void actualiserTicketEnvoye() {
        System.out.println("IHMBar.actualiserTicketEnvoye");

        //Si on n'est pas en train de préparer un ticket on en prend un nouveau
        if(this.ticketEnvoye == null) {
            this.ticketEnvoye = app.getPremierTicketFileBar();
            this.majInterfaceNouveauTicket();
        }
    }
}