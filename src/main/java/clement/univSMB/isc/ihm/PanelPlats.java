package clement.univSMB.isc.ihm;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import clement.univSMB.isc.model.Plat;

public class PanelPlats extends JPanel implements ListSelectionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IHMService p;
	private JList<String> jListInfosPlats;
	private ArrayList<Plat> listePlats;
	
	
	public PanelPlats(IHMService parent) {
		setBounds(new Rectangle(330, 90, 310, 360));
		setLayout(new GridLayout(0, 1));
		this.jListInfosPlats = new JList<String>();
		this.jListInfosPlats.addListSelectionListener(this);
		this.listePlats = new ArrayList<Plat>();
		add(this.jListInfosPlats);
		this.p = parent;
	}
	
	public void updateListInfosPlats(ArrayList<Plat> lPlats)
	{
		this.listePlats = lPlats;
		String[] tabInfos = new String[lPlats.size()];
		for(int i=0; i<tabInfos.length; i++)
		{
			tabInfos[i] = lPlats.get(i).getInfosPlat();
		}
		jListInfosPlats.setListData(tabInfos);
	}

	public void valueChanged(ListSelectionEvent e) {
		if (!e.getValueIsAdjusting()) {
        	int num = jListInfosPlats.getSelectedIndex();
        	if(num != -1) //Lorsque la liste perd le focus, num = -1
			{
        		//Si on regarde les plats et non les menus
        		if( ! p.getPanelPrincipal().getPanelMenus().isVisible() )
        		{
        			p.getPanelPrincipal().getPanelCommande().setSelection( listePlats.get(num) );	
        		}
			}
        }		
	}	
}
