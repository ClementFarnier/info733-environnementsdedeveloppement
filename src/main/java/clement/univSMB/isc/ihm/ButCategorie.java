package clement.univSMB.isc.ihm;

import javax.swing.JButton;

import clement.univSMB.isc.model.Categorie;

public class ButCategorie extends JButton {
	private static final long serialVersionUID = 1L;
	private Categorie catLiee;
	
	public ButCategorie(Categorie catLiee)
	{
		this.catLiee = catLiee;
		setText(catLiee.getNomCategorie());
	}
	
	public Categorie getCatLiee()
	{
		return this.catLiee;
	}
}