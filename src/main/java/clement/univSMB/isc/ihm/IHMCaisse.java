package clement.univSMB.isc.ihm;

import clement.univSMB.isc.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IHMCaisse extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Application app;
    private IHM ihm;
	private Container c;
    private JLabel lblSelectTable;
    private JComboBox cbTables;
    private DefaultListModel lModel;
    private JList jListObjetsCommmandes;
    private JButton btnEncaisser;
    private Table table;
    private JTextField tfTotal;

	public IHMCaisse(IHM ihm)
	{
        this.ihm = ihm;
        this.app = ihm.getApp();
		this.setTitle("ALBF (Caisse)");
	    this.setBounds(800, 340, 290, 380);
	    this.setResizable(false);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.c = this.getContentPane();
	    this.c.setLayout(null);
        this.table = app.getListeTables().get(0);

        lblSelectTable = new JLabel("Reçu pour la table : ");
        lblSelectTable.setBounds(10,10,220,20);
        c.add(lblSelectTable);

        cbTables = new JComboBox();
        for(Table t : app.getListeTables())
        {
            cbTables.addItem(t);
        }
        cbTables.setBounds(10, 40, 270, 30);
        cbTables.addActionListener(this);
        c.add(cbTables);

        lModel = new DefaultListModel<String>();
        jListObjetsCommmandes = new JList(lModel);
        jListObjetsCommmandes.setBounds(10, 80, 270, 190);
        c.add(jListObjetsCommmandes);
        
        btnEncaisser = new JButton("Valider paiement");
        btnEncaisser.addActionListener(this);
        btnEncaisser.setBounds(50, 319, 180, 25);
        getContentPane().add(btnEncaisser);
        
        JLabel lblTotalCommande = new JLabel("Total commande :");
        lblTotalCommande.setBounds(10, 282, 152, 15);
        getContentPane().add(lblTotalCommande);
        
        tfTotal = new JTextField();
        tfTotal.setEditable(false);
        tfTotal.setBounds(165, 282, 65, 19);
        getContentPane().add(tfTotal);
        tfTotal.setColumns(10);
        
        JLabel label = new JLabel("€");
        label.setBounds(236, 282, 29, 15);
        getContentPane().add(label);

	    this.setVisible(true);
	}

    public void reloadJListObjetsCommandes()
    {
        btnEncaisser.setEnabled(false);
        lModel.removeAllElements();

        Commande c = this.table.getCommande();

        if(c == null)
        {
            lModel.addElement("Table libre.");
        }
        else
        {
            for(Plat p : this.table.getCommande().getInfosCommande()) {
                lModel.addElement(p.toString());
            }

            if(c.estTerminee())
            {
                btnEncaisser.setEnabled(true);
            }

            tfTotal.setText( String.valueOf(table.getCommande().getPrixTotal()) );
        }
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == cbTables)
        {
            int idx = cbTables.getSelectedIndex();
            this.table = app.getListeTables().get(idx);
            reloadJListObjetsCommandes();
        }
        else if(e.getSource() == btnEncaisser)
        {
            app.encaisserPaiement( table.getCommande() );
            JOptionPane.showMessageDialog(this, "Paiement validé !");

            //On met à jour l'interface caisse
            reloadJListObjetsCommandes();
            //On met à jour l'interface service
            ihm.getIhmService().getPanelPrincipal().updateListesTables();
            ihm.getIhmService().getPanelPrincipal().getPanelCommande().reloadTable();
        }
    }
}