package clement.univSMB.isc.ihm;

import clement.univSMB.isc.model.*;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args)
    {
        Application app = creerAppTest();
        new IHM(app);
    }

    public static Application creerAppTest()
    {
        /*Ajout des données de test*/
        ArrayList<Plat> lpJour = new ArrayList<Plat>();
        ArrayList<Plat> lpHier = new ArrayList<Plat>();
        ArrayList<Plat> lpSurprise = new ArrayList<Plat>();
        Carte carte = new Carte();
        Categorie c;

        c = new Categorie("Entrées");
        c.ajouterPlat("Salade nicoise", 7.40f);
        lpJour.add(c.getListePlats().get(0));
        c.ajouterPlat("Salade du Chef", 8.50f);
        c.ajouterPlat("Salade verte", 5.10f);
        lpHier.add(c.getListePlats().get(2));
        c.ajouterPlat("Oeufs mimosa", 7.80f);
        carte.ajouterCategorie(c);

        c = new Categorie("Pizzas");
        c.ajouterPlat("Pizza Reine", 11.90f);
        lpJour.add(c.getListePlats().get(0));
        c.ajouterPlat("Pizza 4 fromages", 12.80f);
        c.ajouterPlat("Pizza 4 saisons", 14.50f);
        c.ajouterPlat("Pizza Orientale", 13.20f);
        carte.ajouterCategorie(c);

        c = new Categorie("Poissons");
        c.ajouterPlat("Tapas", 13f);
        lpJour.add(c.getListePlats().get(0));
        c.ajouterPlat("Magicarpe", 500f);
        lpHier.add(c.getListePlats().get(1));
        carte.ajouterCategorie(c);

        c = new Categorie("Viandes");
        c.ajouterPlat("Escalope à la milanaise", 15f);
        c.ajouterPlat("Escalope aux champignons", 16f);
        c.ajouterPlat("Bavette de boeuf", 18f);
        carte.ajouterCategorie(c);

        c = new Categorie("Desserts");
        c.ajouterPlat("Crème brûlée", 6f);
        lpJour.add(c.getListePlats().get(0));
        c.ajouterPlat("Fondant au chocolat", 5.5f);
        lpHier.add(c.getListePlats().get(1));
        c.ajouterPlat("Crumble aux pommes", 6f);
        carte.ajouterCategorie(c);

        c = new Categorie("Boissons");
        c.ajouterPlat("Panaché du chef", 4f);
        lpJour.add(c.getListePlats().get(0));
        lpHier.add(c.getListePlats().get(0));
        c.ajouterPlat("Eau gazeuse", 2f);
        c.ajouterPlat("Coca-Cola", 2.5f);
        carte.ajouterCategorie(c);

        lpSurprise.add(new Plat("???", c, 10f));
        carte.ajouterMenu(new Menu("Plat du jour", lpJour, 17f));
        carte.ajouterMenu(new Menu("Plat d'hier", lpHier, 17f));
        carte.ajouterMenu(new Menu("Plat surprise", lpSurprise, 20f));

        Application app = new Application(carte);

        app.creerTable(2);
        app.creerTable(6);
        app.creerTable(4);

        return app;
    }
}
