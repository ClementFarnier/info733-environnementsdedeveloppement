package clement.univSMB.isc.ihm;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import clement.univSMB.isc.model.Table;

public class PanelPrincipal extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private IHMService p;
	private JButton btnCategories, btnMenus;
	private PanelCategories panelCategories;
	private PanelMenus panelMenus;
	private PanelPlats panelPlats;
	private PanelCommande panelCommande;
	private JMenuBar menuBar;
	private JMenu jmLibre, jmOcc;
	
	public PanelPrincipal(IHMService parent) {
		this.p = parent;
		setBounds(new Rectangle(0, 0, 640, 480));
		setLayout(null);
		
		this.menuBar = new JMenuBar();
		
		jmLibre = new JMenu("Tables libres");
		jmOcc = new JMenu("Tables occupées");
		updateListesTables();
		menuBar.add(jmLibre);
		menuBar.add(jmOcc);

		add(menuBar);
		menuBar.setBounds(0, 0, 960, 20);		
	    
	    this.panelCategories = new PanelCategories(parent);
	    this.panelCategories.setVisible(false);
	    
	    this.panelMenus = new PanelMenus(parent);
	    this.panelMenus.setVisible(false);
	    
	    this.panelPlats = new PanelPlats(parent);
	    
	    this.panelCommande = new PanelCommande(parent);
	    this.panelCommande.setVisible(true);
		
		btnCategories = new JButton("Liste Catégories");
		btnCategories.setBounds(0, 20, 320, 60);
		btnMenus = new JButton("Liste Menus");
		btnMenus.setBounds(320, 20, 320, 60);
		
		add(btnCategories);
		add(btnMenus);
		
	    add(panelCategories);
	    add(panelMenus);
	    add(panelPlats);	 
	    add(panelCommande);
		
		btnCategories.addActionListener(this);
		btnMenus.addActionListener(this);
		this.setVisible(true);
	}
	
	public void updateListesTables()
	{
		jmLibre.removeAll();
		for(Table t : p.getApp().voirTablesLibres())
		{
			JMenuItem jmi = new TableMenuItem(t);
			jmi.addActionListener(this);
			jmLibre.add(jmi);
		}		
		jmOcc.removeAll();
		for(Table t : p.getApp().voirTablesOccupees())
		{
			JMenuItem jmi = new TableMenuItem(t);
			jmi.addActionListener(this);
			jmOcc.add(jmi);
		}	
	}
	

	public PanelCategories getPanelCategories() {
		return panelCategories;
	}


	public PanelMenus getPanelMenus() {
		return panelMenus;
	}


	public PanelPlats getPanelPlats() {
		return panelPlats;
	}


	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnCategories)
		{
			panelCategories.updateListCategories();
			panelMenus.setVisible(false);
			panelCategories.setVisible(true);
		}	
		else if(e.getSource() == btnMenus)
		{
			panelMenus.updateListMenus();
			panelMenus.setVisible(true);
			panelCategories.setVisible(false);
		}
		else if(e.getSource() instanceof TableMenuItem)
		{
			Table tableSelect = ((TableMenuItem)e.getSource()).getTableLiee();
			p.setTableSelectionnee(tableSelect);
			panelCommande.setTableSelectionee(tableSelect);
			p.setSize(960, 480);
			this.setSize(960,480);
			panelCommande.setVisible(true);
		}
	}


	public PanelCommande getPanelCommande() {
		return panelCommande;
	}
}
