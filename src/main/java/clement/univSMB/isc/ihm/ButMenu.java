package clement.univSMB.isc.ihm;

import javax.swing.JButton;

import clement.univSMB.isc.model.Menu;

public class ButMenu extends JButton {
	private static final long serialVersionUID = 1L;
	private Menu menuLie;
	
	public ButMenu(Menu menuLie)
	{
		this.menuLie = menuLie;
		setText(menuLie.getNomMenu());
	}
	
	public Menu getMenuLie()
	{
		return this.menuLie;
	}
}