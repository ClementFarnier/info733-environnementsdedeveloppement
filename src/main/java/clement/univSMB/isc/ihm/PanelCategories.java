package clement.univSMB.isc.ihm;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import clement.univSMB.isc.model.Categorie;

public class PanelCategories extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IHMService p;
	private ArrayList<ButCategorie> listBtn;
	
	
	public PanelCategories(IHMService parent) {
		setBounds(new Rectangle(0, 90, 310, 360));
		setLayout(new GridLayout(0, 2));
		
		this.p = parent;
	}
	
	public void updateListCategories()
	{
		ArrayList<Categorie> lCategories = p.getApp().consulterListeCategories();
		this.removeAll(); //On supprime les anciens boutons
		this.listBtn = new ArrayList<ButCategorie>();
		for(Categorie c : lCategories)
		{
			ButCategorie b = new ButCategorie(c);
			b.setVisible(true);
			add(b);
			b.addActionListener(this);
			listBtn.add(b);
			
		}
	}
	

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof ButCategorie)
		{
			Categorie c = ((ButCategorie)e.getSource()).getCatLiee();
			p.getPanelPrincipal().getPanelPlats().updateListInfosPlats(c.getListePlats());
		}	
	}
}
