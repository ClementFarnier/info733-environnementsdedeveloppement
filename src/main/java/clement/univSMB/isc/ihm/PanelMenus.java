package clement.univSMB.isc.ihm;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import clement.univSMB.isc.model.Menu;

public class PanelMenus extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IHMService p;
	private ArrayList<ButMenu> listBtn;
	
	
	public PanelMenus(IHMService parent) {
		setBounds(new Rectangle(0, 90, 310, 360));
		setLayout(new GridLayout(0, 1));
		
		this.p = parent;
	}
	
	public void updateListMenus()
	{
		ArrayList<Menu> lMenus = p.getApp().consulterListeMenus();
		this.removeAll(); //On supprime les anciens boutons
		this.listBtn = new ArrayList<ButMenu>();
		
		for(Menu m : lMenus)
		{
			ButMenu b = new ButMenu(m);
			b.setVisible(true);
			add(b);
			b.addActionListener(this);
			listBtn.add(b);		
		}
	}
	

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof ButMenu)
		{
			Menu m = ((ButMenu)e.getSource()).getMenuLie();
			p.getPanelPrincipal().getPanelPlats().updateListInfosPlats(m.getListePlats());
			p.getPanelPrincipal().getPanelCommande().setSelection(m);
		}	
	}
}
