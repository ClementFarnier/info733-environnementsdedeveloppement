package clement.univSMB.isc.model;
import java.util.ArrayList;

/**
 * 
 */
public class Application {

    /**
     * Default constructor
     */
    public Application(Carte carteDuRestaurant) {
        this.carteDuRestaurant = carteDuRestaurant;
        this.listeTables = new ArrayList<Table>();
        this.fileAttenteTicketsCuisine = new ArrayList<Ticket>();
        this.fileAttenteTicketsBar = new ArrayList<Ticket>();
    }

    private Carte carteDuRestaurant;
    
    private ArrayList<Table> listeTables;

    private ArrayList<Ticket> fileAttenteTicketsCuisine;
    private ArrayList<Ticket> fileAttenteTicketsBar;
    private clement.univSMB.isc.ihm.IHM ihm;


    public clement.univSMB.isc.ihm.IHM getIHM() {
        return ihm;
    }

    public void setIHM(clement.univSMB.isc.ihm.IHM ihm) {
        this.ihm = ihm;
    }

    public ArrayList<Categorie> consulterListeCategories() {
        return this.carteDuRestaurant.getListeCategories();
    }

    public ArrayList<Menu> consulterListeMenus() {
        return this.carteDuRestaurant.getListeMenus();
    }
    
    public void creerTable(int capacite) {
        assert capacite > 0;
        listeTables.add(new Table(this, listeTables.size() + 1, capacite));
    }

    public ArrayList<Table> voirTablesLibres() {
    	ArrayList<Table> lTlibres = new ArrayList<Table>();
        for(Table t: this.listeTables)
        {
        	if(t.estLibre())
        	{
        		lTlibres.add(t);
        	}
        }
    	return lTlibres;
    }
    
    public ArrayList<Table> voirTablesOccupees() {
    	ArrayList<Table> lToccup = new ArrayList<Table>();
        for(Table t: this.listeTables)
        {
        	if( ! t.estLibre() )
        	{
        		lToccup.add(t);
        	}
        }
    	return lToccup;
    }
    
    public ArrayList<Table> getListeTables() {
        return listeTables;
    }

    public Table voirTable(int numTable) {
        return listeTables.get(numTable);
    }

    public void ajouterTicketFileCuisine(Ticket t) {
        fileAttenteTicketsCuisine.add(t);
    }
    public Ticket getPremierTicketFileCuisine() {
        if(fileAttenteTicketsCuisine.size() > 0)
        {
            return fileAttenteTicketsCuisine.get(0);
        }
        return null;
    }
    public void defilerTicketCuisine() {
        fileAttenteTicketsCuisine.remove(0);
    }

    public void ajouterTicketFileBar(Ticket t) {
        fileAttenteTicketsBar.add(t);
    }
    public Ticket getPremierTicketFileBar() {
        if(fileAttenteTicketsBar.size() > 0)
        {
            return fileAttenteTicketsBar.get(0);
        }
        return null;
    }
    public void defilerTicketBar() {
        fileAttenteTicketsBar.remove(0);
    }



    public void notifierTicketPret(Ticket t) {
        ihm.getIhmService().notifierTicketPret(t);
    }



    public void encaisserPaiement(Commande c) {
        c.getTable().libererTable();
        //On gère le paiement
    }


}