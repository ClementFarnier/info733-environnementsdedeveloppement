package clement.univSMB.isc.model;


import java.util.*;

/**
 * 
 */
public class Menu {

    /**
     * Default constructor
     */
    public Menu(String nomMenu, ArrayList<Plat> listePlats, float prix) {
        this.nomMenu = nomMenu;
        this.listePlats = listePlats;
        this.prix = prix;
    }
    
    public void ajouterPlat(Plat p)
    {
    	listePlats.add(p);
    }

    /**
     * 
     */
    private String nomMenu;
    private float prix;
    private ArrayList<Plat> listePlats;

    public ArrayList<Plat> getListePlats() {
		return listePlats;
	}

	public String getNomMenu()
    {
        return this.nomMenu;
    }

    public float getPrixMenu()
    {
        return prix;
    }

    public ArrayList<String> getInfosMenu() {
        ArrayList<String> arrStr = new ArrayList<String>();

        arrStr.add( nomMenu + " - " + prix + "€");

        for(Plat p : listePlats)
        {
            arrStr.add( p.getNomPlat() );
        }
        return arrStr;
    }

    public String toString() { return nomMenu + " - " + prix + " €"; }

}