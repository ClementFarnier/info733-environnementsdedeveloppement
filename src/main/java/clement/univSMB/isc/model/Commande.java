package clement.univSMB.isc.model;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * 
 */
public class Commande {

    public Commande(Table tableLiee) {
        this.tableLiee = tableLiee;
    	listePlats = new ArrayList<Plat>();
    	listeMenus = new ArrayList<Menu>();
    	listeTickets = new ArrayList<Ticket>();
    }
    
    private ArrayList<Plat> listePlats;
    private ArrayList<Menu> listeMenus;
    private ArrayList<Ticket> listeTickets;


    private Table tableLiee;


    public void ajouterPlat(Plat p) {
        listePlats.add(p);
    }

    public void ajouterMenu(Menu m) {
        listeMenus.add(m);
    }

    public ArrayList<Plat> validerCommande() {
    	ArrayList<Plat> lPlatsEtPlatsMenus = new ArrayList<Plat>(listePlats);
    	
    	for(Menu m : listeMenus)
    	{
    		lPlatsEtPlatsMenus.addAll(m.getListePlats());
    	}    	
    	return lPlatsEtPlatsMenus;    
    }

    /**
     * @param listePlats
     */
    public void creerTicket(Application app, ArrayList<Plat> listePlats, Ticket.TypeTicket type) {
        this.listeTickets.add(new Ticket(this, listePlats, type));
    }


    public Table getTable() { return tableLiee; }

    public void finaliserCommande() {
    	System.out.println("Commande finalisée.");
    }
    
    public String getInfosTickets()
    {
    	String str = "";
    	for(Ticket t : listeTickets)
    	{
    		str += t.toString() + "\n";
    	}
    	return str;
    }

    public ArrayList<Plat> getListePlats() {
		return listePlats;
	}

	public ArrayList<Menu> getListeMenus() {
		return listeMenus;
	}

    public boolean demanderSuite() {

        Ticket t = listeTickets.get(0);

        if(t.getEtat() instanceof  ET_En_Attente)
        {
            t.getEtat().annoterTicket();
            return true;
        }

        //Si le ticket est déjà en traitement
        return false;
    }

    public ArrayList<Ticket> getTickets() {
        return listeTickets;
    }

    //Renvoie toutes les consommations
    public ArrayList<Plat> getInfosCommande()
    {
        ArrayList<Plat> lConso = new ArrayList<Plat>();
        lConso.addAll(listePlats);
        for(Menu m : listeMenus)
        {
            lConso.addAll(m.getListePlats());
        }
        return lConso;
    }

    public boolean estTerminee() { return listeTickets.size() == 0; }

    public float getPrixTotal()
    {
        float prix = 0f;
        for(Plat p: listePlats)
            prix += p.getPrix();
        for(Menu m: listeMenus)
            prix += m.getPrixMenu();
        return prix;
    }

}