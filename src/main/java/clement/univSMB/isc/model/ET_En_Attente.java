package clement.univSMB.isc.model;
import java.util.*;

public class ET_En_Attente extends EtatTicket {

	public ET_En_Attente(Ticket ticket) {
		super(ticket);
	}

	public void annoterTicket() {
		ET_Envoye nouvelEt = new ET_Envoye(ticket);
		ticket.setEtat(nouvelEt);

		if(ticket.getType() == Ticket.TypeTicket.TICKET_CUISINE)
		{
			ticket.getCmd().getTable().getApp().ajouterTicketFileCuisine(ticket);
			ticket.getCmd().getTable().getApp().getIHM().getIhmCuisine().actualiserTicketEnvoye();
		}
		else //Ticket Bar
		{
			ticket.getCmd().getTable().getApp().ajouterTicketFileBar(ticket);
			ticket.getCmd().getTable().getApp().getIHM().getIhmBar().actualiserTicketEnvoye();
		}
	}

}