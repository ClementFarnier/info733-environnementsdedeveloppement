package clement.univSMB.isc.model;

/**
 *
 */
public class Table {
    /**
     *
     */
    private int numero;
    private Application app;
    private int capacite;
    private Commande commandeLiee;

    /**
     * Default constructor
     */
    public Table(Application app, int numTable, int capacite) {
        this.app = app;
        this.numero = numTable;
        this.capacite = capacite;
        this.commandeLiee = null;
    }

    /**
     * @return
     */
    public Commande initialiserCommande() {
        this.commandeLiee = new Commande(this);
        return this.commandeLiee;
    }

    /**
     *
     */
    public Commande getCommande() {
        return commandeLiee;
    }

    public void supprimerCommande() {
        this.commandeLiee = null;
    }

    public Application getApp() { return app; }

    public boolean estLibre() {
        return this.commandeLiee == null;
    }

    public void libererTable() { this.commandeLiee = null; }

    public String toString() {
        return "Table " + numero + " (" + capacite + " pers)";
    }

}