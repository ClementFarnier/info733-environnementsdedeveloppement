package clement.univSMB.isc.model;

/**
 * 
 */
public class Plat {

    /**
     * Default constructor
     */
    public Plat(String nomPlat, Categorie categorieParente, float prix) {
        this.nomPlat = nomPlat;
        this.categorieParente = categorieParente;
        this.prix = prix;
    }

    /**
     * 
     */
    private String nomPlat;



    private float prix;

    /**
     * 
     */
    private Categorie categorieParente;

    public String getNomPlat() { return this.nomPlat; }
    public String getInfosPlat() {
        return this.nomPlat + " - " + prix + "€" ;
    }
    public float getPrix() { return prix; }
    
    public String toString() { return getInfosPlat(); }

	public Categorie getCategorieParente() {
		return categorieParente;
	}

}