package clement.univSMB.isc.model;

import java.util.*;

public abstract class EtatTicket {

	protected Ticket ticket;

	public EtatTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public abstract void annoterTicket();

}