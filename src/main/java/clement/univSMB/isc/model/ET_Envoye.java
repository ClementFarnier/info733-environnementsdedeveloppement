package clement.univSMB.isc.model;
import java.util.*;

public class ET_Envoye extends EtatTicket {

	public ET_Envoye(Ticket ticket) {
		super(ticket);
	}

	public void annoterTicket() {
		ticket.setEtat(new ET_En_Preparation(ticket));
	}

}