package clement.univSMB.isc.model;


import java.util.*;

/**
 * 
 */
public class Categorie {

    /**
     * Default constructor
     */
    public Categorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
        this.listePlats = new ArrayList<Plat>();
    }
    
    public void ajouterPlat(String nomPlat, float prix)
    {
    	listePlats.add(new Plat(nomPlat, this, prix));
    }

    /**
     * 
     */
    private String nomCategorie;

    public String getNomCategorie()
    {
        return this.nomCategorie;
    }

    /**
     * 
     */
    private ArrayList<Plat> listePlats;

    /**
     * @return
     */
    public ArrayList<String> consulterListePlats() {
        ArrayList<String> arrStr = new ArrayList<String>();

        arrStr.add( nomCategorie );

        for(Plat p : listePlats)
        {
            arrStr.add( p.getInfosPlat() );
        }
        return arrStr;
    }

	public ArrayList<Plat> getListePlats() {
		return listePlats;
	}

}