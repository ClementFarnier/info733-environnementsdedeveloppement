package clement.univSMB.isc.model;

import java.util.ArrayList;

public class Ticket {

    public enum TypeTicket {TICKET_CUISINE, TICKET_BAR};

    public Ticket(Commande cmd, ArrayList<Plat> listePlats, Ticket.TypeTicket type) {
        this.cmd = cmd;
        this.listePlats = listePlats;
        this.type = type;
        this.etat = new ET_En_Attente(this);
    }

    private Commande cmd;
    private ArrayList<Plat> listePlats;
    private TypeTicket type;
    private EtatTicket etat;
    
    public String toString()
    {
    	return "Ticket : " + listePlats.toString();
    }

    public EtatTicket getEtat() {
        return etat;
    }

    public void setEtat(EtatTicket etat) {
        this.etat = etat;
    }

    public TypeTicket getType() {
        return type;
    }

    public ArrayList<Plat> getListePlats()
    {
        return listePlats;
    }

    public Commande getCmd() {
        return cmd;
    }

}