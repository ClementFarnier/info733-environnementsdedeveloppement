package clement.univSMB.isc.model;
import java.util.*;

public class ET_En_Preparation extends EtatTicket {

	public ET_En_Preparation(Ticket ticket) {
		super(ticket);
	}

	public void annoterTicket() {
		ticket.setEtat(new ET_Traite(ticket));
		Application app = ticket.getCmd().getTable().getApp();

		app.notifierTicketPret(ticket);
        ticket.getCmd().getTickets().remove(0); //On suppr le ticket dans la commande

		if(ticket.getType() == Ticket.TypeTicket.TICKET_CUISINE)
		{
			app.defilerTicketCuisine();
		}
		else //TICKET_BAR
		{
			app.defilerTicketBar();
		}


		//IHM
		if(ticket.getCmd().estTerminee())
		{
			//On rafraichit l'interface caisse
			app.getIHM().getIhmCaisse().reloadJListObjetsCommandes();
		}
	}

}