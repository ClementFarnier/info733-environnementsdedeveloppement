package clement.univSMB.isc.model;


import java.util.*;

/**
 * 
 */
public class Carte {

    /**
     * Default constructor
     */
    public Carte() {
        this.listeCategories = new ArrayList<Categorie>();
        this.listeMenus = new ArrayList<Menu>();
    }
    
    public void ajouterCategorie(Categorie c)
    {
    	listeCategories.add(c);
    }
    
    public void ajouterMenu(Menu m)
    {
    	listeMenus.add(m);
    }
    /**
     * 
     */
    private ArrayList<Categorie> listeCategories;

    /**
     * 
     */
    private ArrayList<Menu> listeMenus;

    /**
     * @return
     */
    public ArrayList<Categorie> getListeCategories() {
        return this.listeCategories;
    }

    /**
     * @return
     */
    public ArrayList<Menu> getListeMenus() {
        return this.listeMenus;
    }

}